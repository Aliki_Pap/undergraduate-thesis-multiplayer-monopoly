import pygame

CARD_DIST = 30
LEFT_MARGIN = 16
COLOR_GROUP_DIST = 20
OWNED_PROPERTY_WIDTH = 20
OWNED_PROPERTY_HEIGHT = 35
ROW_ONE_Y = 540
Y_DIST = 50

ROW_TWO_Y = ROW_ONE_Y + Y_DIST
ROW_THREE_Y = ROW_TWO_Y + Y_DIST
ROW_FOUR_Y = ROW_THREE_Y + Y_DIST

ROW_START = 765
ROW_ONE_START = ROW_THREE_START = 756 + 16
ROW_MIDDLE = 765 + 100
ROW_ONE_END = ROW_TWO_END = 925 + 50
ROW_THREE_END = ROW_ONE_END + CARD_DIST/2
ROW_TWO_START = 765

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BROWN = (139, 69, 19)
LIGHTBLUE = (179, 242, 255)
PINK = (255, 0, 255)
RED = (255, 0, 0)
DARKRED = (179, 0, 30)
YELLOW = (255, 255, 0)
GREEN = (50, 205, 50)
DARKGREEN = (0, 102, 51)
DARKBLUE = (0, 34, 204)
GREY = (155, 155, 155)
ORANGE = (255, 180, 0)

DOG = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/dog.png")
HAT = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/hat.png")
LIGHTBULB_IMG = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/lightbulb.png")
WATERDROP_IMG = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/water_droplet.png")
STATHMOS_IMG = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/stathmos_icon.jpeg")
stathmos_img_rect = STATHMOS_IMG.get_rect()
player_tokens = [DOG, HAT]


class Player:

    owned_properties = set()

    def __init__(self, name, money, x, y, turn_number):
        self.name = name
        self.money = money
        self.properties = set()
        self.color_freq = {}
        self.houses = {}
        self.turn_number = turn_number

        self.position = 0
        self.x = x
        self.y = y

        self.in_jail = False
        self.jail_turns = 0

    def draw(self, window):
        window.blit(player_tokens[self.turn_number], (self.x, self.y))

    def make_deposit(self, amount):
        self.money += amount

    def move(self, dice_roll):
        temp_pos = self.position
        self.position += dice_roll
        self.position = self.position % 40
        if temp_pos > self.position:
            self.make_deposit(200)  # passed GO so player gets 200$

        while temp_pos != self.position:
            if temp_pos < 10:
                self.x += 56 * -1
                self.y = 670
            elif 10 <= temp_pos < 20:
                self.y += 56 * -1
                self.x = 70
            elif 20 <= temp_pos < 30:
                self.x += 56
                self.y = 90
            elif 30 <= temp_pos < 40:
                self.y += 56
                self.x = 640

            temp_pos += 1
            temp_pos = temp_pos % 40

    def add_property(self, property_id, property_color):
        self.properties.add(property_id)
        if property_color in self.color_freq:
            self.color_freq[property_color] += 1
        else:
            self.color_freq[property_color] = 1

    def buy_property(self, property_id, property_color, cost):
        if self.money - cost >= 0 and property_id not in Player.owned_properties:
            Player.owned_properties.add(property_id)
            self.add_property(property_id, property_color)
            self.money -= cost

    def get_property_house_amount(self, property_id):
        if property_id not in self.houses:
            return 0
        return self.houses[property_id]

    def buy_house(self, property_id, cost, color_property, color_property2=None):
        if self.money - cost >= 0:
            if property_id in self.properties:
                if color_property2 is not None:
                    if (self.get_property_house_amount(property_id) - self.get_property_house_amount(color_property)) < 1 \
                        and (self.get_property_house_amount(property_id) - self.get_property_house_amount(color_property2)) < 1 \
                        and self.get_property_house_amount(property_id) < 5:
                        self.houses[property_id] += 1
                        self.money -= cost
                    elif (self.get_property_house_amount(property_id) - self.get_property_house_amount(color_property)) < 1 \
                        and self.get_property_house_amount(property_id) < 5:
                        self.houses[property_id] += 1
                        self.money -= cost
                else:
                    self.houses[property_id] = 1
                    self.money -= cost

    def property_owned(self, property_id):
        return property_id in self.properties

    def pay(self, amount):
        if self.money - amount > 0:
            self.money -= amount

    def can_afford(self, amount):
        if self.money - amount >= 0:
            return True
        return False

    def remove_property(self, property_id, property_color):
        self.properties.remove(property_id)
        self.color_freq[property_color] -= 1

    def has_color_group(self, color):
        if color in self.color_freq:
            if (color == "Brown" or color == "Dark Blue") and self.color_freq[color] == 2:
                return True
            elif self.color_freq[color] == 3:
                return True
        return False

    def get_number_group_owned(self, group):
        return self.color_freq[group]

    def get_position(self):
        return self.position

    def set_position(self, pos, x, y):
        self.position = pos
        self.x = x
        self.y = y

    def go_jail(self):
        self.set_position(10, 70, 680)
        self.in_jail = True

    def leave_jail(self):
        self.in_jail = False
        self.jail_turns = 0

    def increment_jail(self):
        if self.in_jail:
            self.jail_turns += 1

    def get_jail_count(self):
        return self.jail_turns

    def is_in_jail(self):
        return self.in_jail

    def is_player_turn(self, turn):
        if self.turn_number == turn:
            return True
        return False

    def get_turn_number(self):
        return self.turn_number

    def get_money(self):
        return self.money

    def get_properties(self):
        return self.properties

    def get_houses(self):
        return self.houses

    def get_color_frequency(self):
        return self.color_freq

    def get_name(self):
        return self.name

    def get_x(self):
        return self.x

    def get_y(self):
        return self.y

    def draw_houses(self, window, property_id, rect):
        if 0 < self.get_property_house_amount(property_id) < 5:
            house = pygame.Rect(rect.x, rect.y, OWNED_PROPERTY_WIDTH/4, OWNED_PROPERTY_WIDTH/4)
            for x in range(self.get_property_house_amount(property_id)):
                pygame.draw.rect(window, DARKGREEN, house)
                house.topleft = house.topright
        elif self.get_property_house_amount(property_id) == 5:
            hotel = pygame.Rect(rect.x, rect.y, OWNED_PROPERTY_WIDTH, 5)
            pygame.draw.rect(window, DARKRED, hotel)

    def draw_owned_property(self, window, rect, pos, color):
        if self.property_owned(pos):
            pygame.draw.rect(window, color, rect)
            self.draw_houses(window, pos, rect)
        else:
            pygame.draw.rect(window, color, rect, 2)

    def draw_player_properties(self, win):

        # SXEDIASMOS KARTWN POLEWN

        rect = pygame.Rect(ROW_ONE_START, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 1, BROWN)  # Odos_Kypselis

        rect = pygame.Rect(ROW_ONE_START + CARD_DIST, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 3, BROWN)  # Odos_Eptanisou

        rect = pygame.Rect(ROW_MIDDLE, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 6, LIGHTBLUE)  # Plateia_Vathis

        rect = pygame.Rect(ROW_MIDDLE + CARD_DIST, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 8, LIGHTBLUE)  # Odos_Liosiwn

        rect = pygame.Rect(ROW_MIDDLE + 2*CARD_DIST, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 9, LIGHTBLUE)  # Odos_Aharnwn

        rect = pygame.Rect(ROW_ONE_END, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 11, PINK)  # Plateia_Amerikis

        rect = pygame.Rect(ROW_ONE_END + CARD_DIST, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 13, PINK)  # Plateia_Kaniggos

        rect = pygame.Rect(ROW_ONE_END + 2*CARD_DIST, ROW_ONE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 14, PINK)  # Odos_Patisiwn

        rect = pygame.Rect(ROW_TWO_START, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 16, ORANGE)  # Odos_Peiraiws

        rect = pygame.Rect(ROW_TWO_START + CARD_DIST, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 18, ORANGE)  # Odos_Athinas

        rect = pygame.Rect(ROW_TWO_START + 2*CARD_DIST, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 19, ORANGE)  # Odos_Aiolou

        rect = pygame.Rect(ROW_MIDDLE, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 21, RED)  # Odos_Praxitelous

        rect = pygame.Rect(ROW_MIDDLE + CARD_DIST, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 23, RED)  # Odos_Korah

        rect = pygame.Rect(ROW_MIDDLE + 2*CARD_DIST, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 24, RED)  # Plateia_Omonoias

        rect = pygame.Rect(ROW_TWO_END, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 26, YELLOW)  # Odos_Stadiou

        rect = pygame.Rect(ROW_TWO_END + CARD_DIST, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 27, YELLOW)  # Odos_Panepistimiou

        rect = pygame.Rect(ROW_TWO_END + 2*CARD_DIST, ROW_TWO_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 29, YELLOW)  # Plateia_Syntagmatos

        rect = pygame.Rect(ROW_MIDDLE, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 31, GREEN)  # Odos_Akadimias

        rect = pygame.Rect(ROW_MIDDLE + CARD_DIST, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 32, GREEN)  # Odos_Skoufa

        rect = pygame.Rect(ROW_MIDDLE + 2*CARD_DIST, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 34, GREEN)  # Odos_Hrodotou

        rect = pygame.Rect(ROW_THREE_END, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 37, DARKBLUE)  # Lewforos_Bas_Sofias

        rect = pygame.Rect(ROW_THREE_END + CARD_DIST, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
        self.draw_owned_property(win, rect, 39, DARKBLUE)  # Lewforos_Amalias

        # SXEDIASMOS KARTWN YPOLOIPWN

        if self.property_owned(12):  # Etaireia_Hlektrismou
            electric_company_rect = pygame.Rect(ROW_THREE_START, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            lightbulb_rect = LIGHTBULB_IMG.get_rect()
            lightbulb_rect.center = electric_company_rect.center
            pygame.draw.rect(win, WHITE, electric_company_rect)
            win.blit(LIGHTBULB_IMG, lightbulb_rect)
        else:
            electric_company_rect = pygame.Rect(ROW_THREE_START, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, WHITE, electric_company_rect, 2)

        if self.property_owned(28):  #Etaireia_Ydreusis
            water_company_rect = pygame.Rect(ROW_THREE_START + CARD_DIST, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            waterdrop_rect = WATERDROP_IMG.get_rect()
            waterdrop_rect.center = water_company_rect.center
            pygame.draw.rect(win, WHITE, water_company_rect)
            win.blit(WATERDROP_IMG, waterdrop_rect)
        else:
            water_company_rect = pygame.Rect(ROW_THREE_START + CARD_DIST, ROW_THREE_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, WHITE, water_company_rect, 2)

        if self.property_owned(5): #Stathmos_Peloponnisou
            st_pelopon_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_pelopon_rect)
            stathmos_img_rect.center = st_pelopon_rect.center
            win.blit(STATHMOS_IMG, stathmos_img_rect)
        else:
            st_pelopon_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_pelopon_rect, 2)

        if self.property_owned(15): #Stathmos_Tatoiou
            st_tatoiou_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2 + CARD_DIST, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_tatoiou_rect)
            stathmos_img_rect.center = st_tatoiou_rect.center
            win.blit(STATHMOS_IMG, stathmos_img_rect)
        else:
            st_tatoiou_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2 + CARD_DIST, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_tatoiou_rect, 2)

        if self.property_owned(25): #Stathmos_Larisis
            st_larisis_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2 + 2*CARD_DIST, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_larisis_rect)
            stathmos_img_rect.center = st_larisis_rect.center
            win.blit(STATHMOS_IMG, stathmos_img_rect)
        else:
            st_larisis_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2 + 2*CARD_DIST, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_larisis_rect, 2)

        if self.property_owned(35): #Stathmos_Peiraiws
            st_peiraiws_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2 + 3*CARD_DIST, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_peiraiws_rect)
            stathmos_img_rect.center = st_peiraiws_rect.center
            win.blit(STATHMOS_IMG, stathmos_img_rect)
        else:
            st_peiraiws_rect = pygame.Rect(ROW_MIDDLE - CARD_DIST/2 + 3*CARD_DIST, ROW_FOUR_Y, OWNED_PROPERTY_WIDTH, OWNED_PROPERTY_HEIGHT)
            pygame.draw.rect(win, GREY, st_peiraiws_rect, 2)

