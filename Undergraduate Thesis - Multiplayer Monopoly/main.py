import pygame
import json
import random

from player import Player
from Board import Board
import GUI
import Pygame_Textinput

pygame.init()

# Setting up pygame
WINDOW_WIDTH, WINDOW_HEIGHT = 1250, 800
WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
FPS = 60
pygame.display.set_caption("Web Monopoly")

FONT = pygame.font.Font(None, 30)
SMALLFONT = pygame.font.Font(None, 20)

# Define Colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREY = (155, 155, 155)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
ORANGE = (255, 180, 0)
BUTTON_COLOR = (114, 9, 183)


BOARD_IMG = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/board.jpg")
board_rect = BOARD_IMG.get_rect()
board_rect.center = (BOARD_IMG.get_height() / 2 + 50, WINDOW_HEIGHT / 2)

DOG = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/dog.png")
HAT = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/hat.png")
START_X, START_Y = 640, 670

# Property Images:
property_img = dict({

    1: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Kypselis.jpg"),
    3: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Eptanisou.jpg"),
    5: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Peloponnisou.jpg"),
    6: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Vathis.jpg"),
    8: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Liosiwn.jpg"),
    9: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Aharnwn.jpg"),
    11: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Amerikis.jpg"),
    12: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Etaireia_Hlektrismou.jpg"),
    13: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Kaniggos.jpg"),
    14: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Patisiwn.jpg"),
    15: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Tatoiou.jpg"),
    16: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Peiraiws.jpg"),
    18: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Athinas.jpg"),
    19: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Aiolou.jpg"),
    21: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Praxitelous.jpg"),
    23: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Korah.jpg"),
    24: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Omonoias.jpg"),
    25: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Larisis.jpg"),
    26: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Stadiou.jpg"),
    27: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Panepistimiou.jpg"),
    28: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Etaireia_Ydreushs.jpg"),
    29: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Syntagmatos.jpg"),
    31: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Akadimias.jpg"),
    32: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Skoufa.jpg"),
    34: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Hrodotou.jpg"),
    35: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Peiraiws.jpg"),
    37: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Lewforos_Bas_Sofias.jpg"),
    39: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Lewforos_Amalias.jpg")

})

with open('data.json', encoding="utf8") as json_file:
    property_dict = json.load(json_file)  # Contains all the info about each property (name, rent, cost...)

special_locations = {"Αφετηρία", "Απόφαση", "Εντολή", "Φόρος", "Φυλακή", "Ελεύθερη Στάθμευση", "Αστυνόμος"}


class GameEngine:
    """Handles game logic such as jail, rolling doubles, turns, rent etc"""

    def __init__(self, player_count):
        """Initialize essential logic to the game such as players, turns and dice"""

        self.player_count = player_count
        self.turn = 0  # Starts on 1st players turn
        self.rolled = False  # Determines which phase of a players turn (rolling, trading...)
        self.doubles = 0  # Amount of doubles rolled
        self.dice_roll = 0  # Dice roll for single turn

    def change_turn(self):
        self.turn += 1
        self.turn = self.turn % self.player_count  # make sure that the turn loops based on amount of players
        self.rolled = False
        self.doubles = 0
        print(self.turn)

    def get_turn(self):
        return self.turn

    def check_pos(self):
        for player in players:
            if player.is_player_turn(self.turn):
                if player.get_position() == 30:
                    player.go_jail()
                elif player.get_position() == 4:
                    print("TAX 200$!")
                    player.pay(200)
                elif player.get_position() == 38:
                    print("TAX 100$!")
                    player.pay(100)
                elif player.get_position() == 2 or player.get_position() == 17 or player.get_position() == 33:  #Apofasi
                    pass
                elif player.get_position() == 7 or player.get_position() == 22 or player.get_position() == 36:  # Entoli
                    pass

                for other_player in players:
                    if other_player is not player and other_player.property_owned(player.get_position()):
                        if player.get_position() == 12:
                            if other_player.property_owned(28):
                                rent = 10 * self.dice_roll
                            else:
                                rent = 4 * self.dice_roll

                        elif player.get_position() == 28:
                            if other_player.property_owned(12):
                                rent = 10 * self.dice_roll
                            else:
                                rent = 4 * self.dice_roll

                        elif player.get_position() == 5 or player.get_position() == 15 or player.get_position() == 25 or player.get_position() == 35:
                            number_of_railroads = other_player.get_number_group_owned("RR")
                            if number_of_railroads == 1:
                                rent = property_dict["locations"][player.get_position()]["rent"]
                            elif number_of_railroads == 2:
                                rent = property_dict["locations"][player.get_position()]["rent1"]
                            elif number_of_railroads == 3:
                                rent = property_dict["locations"][player.get_position()]["rent2"]
                            elif number_of_railroads == 4:
                                rent = property_dict["locations"][player.get_position()]["rent3"]

                        else:
                            rent = property_dict["locations"][player.get_position()]["rent"]
                            if other_player.has_color_group(property_dict["locations"][player.get_position()]["color"]):
                                number_of_houses = other_player.get_property_house_amount(player.get_position())
                                if number_of_houses == 0:
                                    rent = 2 * rent
                                elif number_of_houses == 1:
                                    rent = property_dict["locations"][player.get_position()]["rent1"]
                                elif number_of_houses == 2:
                                    rent = property_dict["locations"][player.get_position()]["rent2"]
                                elif number_of_houses == 3:
                                    rent = property_dict["locations"][player.get_position()]["rent3"]
                                elif number_of_houses == 4:
                                    rent = property_dict["locations"][player.get_position()]["rent4"]
                                elif number_of_houses == 5:
                                    rent = property_dict["locations"][player.get_position()]["rent5"]

                        other_player.make_deposit(rent)
                        player.pay(rent)
                        break
                break

    def set_dice_roll(self, dice_roll, double):
        self.dice_roll = dice_roll
        for player in players:
            if player.is_player_turn(self.turn):
                player.move(dice_roll)  # Move the player according to the sum of dice rolls
                break

        if double:
            self.doubles += 1
            if self.doubles == 3:   # When three doubles are rolled, player goes to jail
                for player in players:
                    if player.is_player_turn(self.turn):
                        player.go_jail()
                        break
                self.doubles == 0
                self.rolled = True
        else:
            self.rolled = True

        self.check_pos()

    def roll_complete(self):
        return self.rolled

    def is_player_in_jail(self):
        for player in players:
            if player.is_player_turn(game.turn):
                player.increment_jail()
                if player.get_jail_count() > 3:
                    player.leave_jail()
                return player.is_in_jail()


class Dice:

    """Represents the dice within the monopoly game and handles both drawing and rolling"""

    def __init__(self, dice_rect, dice2_rect, color, fontsize):
        """Initialize class variables and the surfaces for drawing the numbers"""
        self.dice_one = '?'
        self.dice_two = '?'
        self.dice_rect = dice_rect
        self.dice2_rect = dice2_rect

        self.color = color
        self.fontsize = fontsize

        self.surface = FONT.render(self.dice_one, True, ORANGE)
        self.surface2 = FONT.render(self.dice_two, True, ORANGE)

        self.text_rect = self.surface.get_rect()
        self.text_rect.center = self.dice_rect.center
        self.text2_rect = self.surface2.get_rect()
        self.text2_rect.center = self.dice2_rect.center

    def roll(self):
        self.dice_one = str(random.randint(1, 6))
        self.dice_two = str(random.randint(1, 6))
        self.surface = FONT.render(self.dice_one, True, ORANGE)
        self.surface2 = FONT.render(self.dice_two, True, ORANGE)

    def is_double(self):
        if self.dice_one == self.dice_two:
            return True
        return False

    def draw_dice(self):
        pygame.draw.rect(WINDOW, self.color, self.dice_rect, 2)
        pygame.draw.rect(WINDOW, self.color, self.dice2_rect, 2)
        WINDOW.blit(self.surface, self.text_rect)
        WINDOW.blit(self.surface2, self.text2_rect)

    def dice_sum(self):
        return int(self.dice_one) + int(self.dice_two)


roll_button = GUI.Button(WINDOW, 'Roll', FONT, (247, 37, 133), pygame.Rect(1110, 200, 100, 60), BUTTON_COLOR)
buy_button = GUI.Button(WINDOW, 'Buy', FONT, (247, 37, 133), pygame.Rect(1110, 280, 100, 60), BUTTON_COLOR)
build_button = GUI.Button(WINDOW, 'Build', FONT, (247, 37, 133), pygame.Rect(1110, 360, 100, 60), BUTTON_COLOR)
end_turn_button = GUI.Button(WINDOW, 'End Turn', FONT, (247, 37, 133), pygame.Rect(1110, 600, 100, 60), BUTTON_COLOR)
bail_button = GUI.Button(WINDOW, 'Pay Bail', FONT, (0, 0, 255), pygame.Rect(1110-225, 200, 100, 60), (255, 0, 0))

textbox = Pygame_Textinput.TextInput("", font_size=20, text_color=WHITE,
                                     cursor_color=(247, 37, 133), max_string_length=22, rect=pygame.Rect(920, 350, 115, 30))
textbox2 = Pygame_Textinput.TextInput("", font_size=20, text_color=WHITE,
                                      cursor_color=(247, 37, 133), max_string_length=18, rect=pygame.Rect(920, 385, 110, 30))

dog = Player("Dog", 40000, START_X, START_Y, 0)
hat = Player("Hat", 40000, START_X, START_Y, 1)
players = [dog, hat]

options = []
game = GameEngine(2)

for player in players:
    options.append(player.get_name())
print(options)

player_option = GUI.OptionBox(760, 435, 310, 30, GREY, (100, 200, 255), FONT, options)
options2 = options.copy()
options2.pop(0)
player_option.print_option_list()

die = Dice(pygame.Rect(1100 + 30, 50, 60, 60), pygame.Rect(1100 + 30, 120, 60, 60), BUTTON_COLOR, 30)


def draw_player_info(selected_option):
    info_rect = pygame.Rect(760, 435, 310, 300)
    pygame.draw.rect(WINDOW, (76, 201, 240), info_rect, 2)
    for player in players:
        if selected_option == player.get_turn_number():
            GUI.draw_text(WINDOW, "Wealth: $" + str(player.get_money()), FONT, WHITE, info_rect.left, 475, False)
            GUI.draw_text(WINDOW, "Properties: ", FONT, WHITE, info_rect.left, 510, False)
            player.draw_player_properties(WINDOW)
            break


def draw_turn_info():
    for player in players:
        if player.is_player_turn(game.get_turn()):
            GUI.draw_text(WINDOW, "Turn: " + player.get_name(), FONT, WHITE, 50, 30, False)
            break


def draw_widgets():
    roll_button.draw()
    die.draw_dice()
    buy_button.draw()
    build_button.draw()
    end_turn_button.draw()

    if (game.get_turn() == 0 and dog.is_in_jail()) or (game.get_turn() == 1 and hat.is_in_jail()):
        bail_button.draw()

    WINDOW.blit(textbox.get_surface(), (textbox.get_rect().left, textbox.get_rect().centery))
    WINDOW.blit(textbox2.get_surface(), (textbox2.get_rect().left, textbox2.get_rect().centery))
    player_option.draw(WINDOW)

    for player in players:
        if player.is_player_turn(game.get_turn()):
            GUI.draw_text(WINDOW, player.get_name(), FONT, (247, 37, 133), 825, 380, True)


def draw(selected_option):
    WINDOW.fill((58, 12, 163))

    draw_turn_info()

    WINDOW.blit(BOARD_IMG, board_rect)
    WINDOW.blit(DOG, (dog.get_x(), dog.get_y()))
    WINDOW.blit(HAT, (hat.get_x(), hat.get_y()))
    draw_player_info(selected_option)
    draw_widgets()

    if game.get_turn() == 0 and dog.get_position() in property_img:
        WINDOW.blit(property_img[dog.get_position()], (785, 60))
    elif game.get_turn() == 1 and hat.get_position() in property_img:
        WINDOW.blit(property_img[hat.get_position()], (785, 60))


def is_money(m):
    if len(m) > 0 and m[0] == "$":
        return True
    return False


def get_property_id(property_name):
    counter = 0
    for index in property_dict["locations"]:
        if index["name"] == property_name:
            break
        counter += 1
    return counter


def main():
    clock = pygame.time.Clock()
    run = True

    while run:
        clock.tick(FPS)
        events = pygame.event.get()
        selected_option = player_option.update(events)

        for event in events:
            if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                if roll_button.clicked(event) and not game.roll_complete() and not game.is_player_in_jail():
                    die.roll()
                    game.set_dice_roll(die.dice_sum(), die.is_double())

                elif buy_button.clicked(event):
                    for player in players:
                        loc = property_dict["locations"][player.get_position()]["name"]
                        if player.is_player_turn(game.get_turn()) and loc not in special_locations:
                            pos = player.get_position()
                            color = property_dict["locations"][pos]["color"]
                            cost = property_dict["locations"][pos]["cost"]
                            player.buy_property(pos, color, cost)
                            break

                elif build_button.clicked(event):
                    pos = get_property_id(textbox.get_text())
                    print(pos)

                    if len(property_dict["locations"]) > pos and "house_price" in property_dict["locations"][pos]:
                        color = property_dict["locations"][pos]["color"]
                        for player in players:
                            if player.is_player_turn(game.get_turn()) and player.has_color_group(color):
                                color_property = property_dict["locations"][pos]["group_id"]
                                house_price = property_dict["locations"][pos]["house_price"]
                                if "group_id2" in property_dict["locations"][pos]:
                                    color_property2 = property_dict["locations"][pos]["group_id2"]
                                    player.buy_house(pos, color, house_price, color_property, color_property2)
                                else:
                                    player.buy_house(pos, color, house_price, color_property)
                                break

                elif textbox.is_clicked(event):
                    textbox.set_enabled(True)

                elif textbox2.is_clicked(event):
                    textbox2.set_enabled(True)

                elif end_turn_button.clicked(event): # and game.roll_complete():
                    game.change_turn()
                    option_list = []
                    for player in players:
                        if player.is_player_turn(game.get_turn()):
                            player_option.set_selected_option(player.get_turn_number())
                        else:
                            option_list.append(player.get_name())

                elif bail_button.clicked(event):
                    for player in players:
                        if player.is_player_turn(game.get_turn()) and player.is_in_jail():
                            player.pay(50)
                            player.leave_jail()
                            break

                elif event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                    if textbox.is_enabled():
                        textbox.set_enabled(False)
                    elif textbox2.is_enabled():
                        textbox2.set_enabled(False)

                elif event.type == pygame.QUIT:
                    run = False

        if textbox.is_enabled():
            textbox.update(events)
        elif textbox2.is_enabled():
            textbox2.update(events)

        draw(selected_option)

        pygame.display.flip()

    pygame.quit()


if __name__ == "__main__":
    main()







