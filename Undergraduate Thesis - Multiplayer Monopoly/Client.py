import pygame
# import os
import json
from Network import Network
from player import Player
# import sys
import Dice
import GUI
import Pygame_Textinput
import random

pygame.init()

# Setting up Pygame
WINDOW_WIDTH, WINDOW_HEIGHT = 1250, 800
WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
FPS = 60
pygame.display.set_caption("Multiplayer Monopoly")

FONT = pygame.font.Font(None, 30)
SMALLFONT = pygame.font.Font(None, 20)

# Define Colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREY = (155, 155, 155)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
ORANGE = (255, 180, 0)

BUTTON_COLOR = (204, 255, 255)

# Setup images
BOARD_IMG = pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/board.jpg")
board_rect = BOARD_IMG.get_rect()
board_rect.center = (int(BOARD_IMG.get_height() / 2 + 50), int(WINDOW_HEIGHT / 2))

# Property Images:
property_img = dict({

    1: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Kypselis.jpg"),
    3: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Eptanisou.jpg"),
    5: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Peloponnisou.jpg"),
    6: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Vathis.jpg"),
    8: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Liosiwn.jpg"),
    9: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Aharnwn.jpg"),
    11: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Amerikis.jpg"),
    12: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Etaireia_Hlektrismou.jpg"),
    13: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Kaniggos.jpg"),
    14: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Patisiwn.jpg"),
    15: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Tatoiou.jpg"),
    16: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Peiraiws.jpg"),
    18: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Athinas.jpg"),
    19: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Aiolou.jpg"),
    21: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Praxitelous.jpg"),
    23: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Korah.jpg"),
    24: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Omonoias.jpg"),
    25: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Larisis.jpg"),
    26: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Stadiou.jpg"),
    27: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Panepistimiou.jpg"),
    28: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Etaireia_Ydreushs.jpg"),
    29: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Plateia_Syntagmatos.jpg"),
    31: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Akadimias.jpg"),
    32: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Skoufa.jpg"),
    34: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Odos_Hrodotou.jpg"),
    35: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Stathmos_Peiraiws.jpg"),
    37: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Lewforos_Bas_Sofias.jpg"),
    39: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Lewforos_Amalias.jpg")

})

# Card Images:
cards_img = dict({

    0: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Entoli id0.jpg"),
    1: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Entoli id1.jpg"),
    2: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Entoli id2.jpg"),
    3: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Entoli id3.jpg"),
    4: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Entoli id4.jpg"),
    5: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Apofasi id5.jpg"),
    6: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Apofasi id6.jpg"),
    7: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Apofasi id7.jpg"),
    8: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Apofasi id8.jpg"),
    9: pygame.image.load("/Users/Aliki/Desktop/Ptuxiaki/images/Apofasi id9.jpg")
})

with open('data.json', encoding="utf8") as locations:
    property_dict = json.load(locations)  # Contains all the info about each property (name, rent, cost...)

special_locations = {"Αφετηρία", "Απόφαση", "Εντολή", "Φόρος", "Φυλακή", "Ελεύθερη Στάθμευση", "Αστυνόμος"}

with open('cards_data.json', encoding="utf8") as cards:
    cards_dict = json.load(cards)   # Contains all the info about each card


class GameEngine:
    """Handles game logic such as jail, rolling doubles, turns, rent etc"""

    def __init__(self, player_count):
        """Initialize essential logic to the game such as players, turns and dice"""

        self.player_count = player_count
        self.turn = 0  # Starts on 1st players turn
        self.rolled = False  # Determines which phase of a players turn (rolling, trading...)
        self.doubles = 0  # Amount of doubles rolled
        self.dice_roll = 0  # Dice roll for single turn

    def change_turn(self):
        self.turn += 1
        self.turn = self.turn % self.player_count  # make sure that the turn loops based on amount of players
        self.rolled = False
        self.doubles = 0

    def set_turn(self, turn):
        self.turn = turn % self.player_count

    def get_turn(self):
        return self.turn

    def check_pos(self, player, players):
        if player.is_player_turn(self.turn):
            if player.get_position() == 30:
                player.go_jail()
            elif player.get_position() == 4:
                print("TAX 200$!")
                player.pay(200)
            elif player.get_position() == 38:
                print("TAX 100$!")
                player.pay(100)

            elif player.get_position() == 2 or player.get_position() == 17 or player.get_position() == 33:  # Apofasi
                number = random.randint(5, 9)
                WINDOW.blit(cards_img[number], (785, 60))
                pygame.display.update()
                pygame.time.delay(3000)
                if number == 5:
                    player.make_deposit(100)
                    return
                elif number == 6:
                    player.pay(100)
                    return
                elif number == 7:
                    player.pay(50)
                    return
                elif number == 8:
                    player.make_deposit(100)
                    return
                else:
                    player.make_deposit(25)
                    return

            elif player.get_position() == 7 or player.get_position() == 22 or player.get_position() == 36:  # Entoli
                number = random.randint(0, 4)
                WINDOW.blit(cards_img[number], (785, 60))
                pygame.display.update()
                pygame.time.delay(3000)
                if number == 0:
                    player.pay(15)
                    return
                elif number == 1:
                    player.pay(50)
                    return
                elif number == 2:
                    player.make_deposit(150)
                    return
                elif number == 3:
                    player.go_jail()
                    return
                else:
                    player.make_deposit(50)
                    return

            for other_player in list(players):
                if other_player is not player and other_player.property_owned(player.get_position()):
                    if player.get_position() == 12:
                        if other_player.property_owned(28):
                            rent = 10 * self.dice_roll
                        else:
                            rent = 4 * self.dice_roll

                    elif player.get_position() == 28:
                        if other_player.property_owned(12):
                            rent = 10 * self.dice_roll
                        else:
                            rent = 4 * self.dice_roll

                    elif player.get_position() == 5 or player.get_position() == 15 or player.get_position() == 25 or player.get_position() == 35:
                        number_of_railroads = other_player.get_number_group_owned("RR")
                        if number_of_railroads == 1:
                            rent = property_dict["locations"][player.get_position()]["rent"]
                        elif number_of_railroads == 2:
                            rent = property_dict["locations"][player.get_position()]["rent1"]
                        elif number_of_railroads == 3:
                            rent = property_dict["locations"][player.get_position()]["rent2"]
                        elif number_of_railroads == 4:
                            rent = property_dict["locations"][player.get_position()]["rent3"]

                    else:
                        rent = property_dict["locations"][player.get_position()]["rent"]
                        if other_player.has_color_group(property_dict["locations"][player.get_position()]["color"]):
                            number_of_houses = other_player.get_property_house_amount(player.get_position())
                            if number_of_houses == 0:
                                rent = 2 * rent
                            elif number_of_houses == 1:
                                rent = property_dict["locations"][player.get_position()]["rent1"]
                            elif number_of_houses == 2:
                                rent = property_dict["locations"][player.get_position()]["rent2"]
                            elif number_of_houses == 3:
                                rent = property_dict["locations"][player.get_position()]["rent3"]
                            elif number_of_houses == 4:
                                rent = property_dict["locations"][player.get_position()]["rent4"]
                            elif number_of_houses == 5:
                                rent = property_dict["locations"][player.get_position()]["rent5"]

                    other_player.make_deposit(rent)
                    player.pay(rent)

    def set_dice_roll(self, dice_roll, double, player):
        self.dice_roll = dice_roll
        print(player.get_name() + " : " + str(player.get_turn_number()) + str(self.turn) + str(self.dice_roll))
        if player.is_player_turn(self.turn):
            print("ABOUT TO MOVE")
            player.move(dice_roll)  # Move the player according to the sum of dice rolls

        if double:
            self.doubles += 1
            if self.doubles == 3:   # When three doubles are rolled, player goes to jail
                if player.is_player_turn(self.turn):
                    player.go_jail()
                self.doubles == 0
                self.rolled = True
        else:
            self.rolled = True

    def set_dice(self, dice_roll):
        self.dice_roll = dice_roll

    def roll_complete(self):
        return self.rolled

    def is_player_in_jail(self, player):
        if player.is_player_turn(game.turn):
            player.increment_jail()
            if player.get_jail_count() > 3:
                player.leave_jail()
            return player.is_in_jail()

    def get_player_amount(self):
        return self.player_count


# GUI COMPONENTS
options = []
game = GameEngine(2)

roll_button = GUI.Button(WINDOW, 'Roll', FONT, ORANGE, pygame.Rect(1110, 200, 100, 60), BUTTON_COLOR)
buy_button = GUI.Button(WINDOW, 'Buy', FONT, ORANGE, pygame.Rect(1110, 280, 100, 60), BUTTON_COLOR)
build_button = GUI.Button(WINDOW, 'Build', FONT, ORANGE, pygame.Rect(1110, 360, 100, 60), BUTTON_COLOR)
end_turn_button = GUI.Button(WINDOW, 'End Turn', FONT, ORANGE, pygame.Rect(1110, 440, 100, 60), BUTTON_COLOR)
bail_button = GUI.Button(WINDOW, 'Pay Bail', FONT, (0, 0, 255), pygame.Rect(1110-225, 200, 100, 60), (255, 0, 0))

textbox = Pygame_Textinput.TextInput("", font_size=20, text_color=WHITE, cursor_color=ORANGE, max_string_length=22, rect=pygame.Rect(920, 350, 115, 30))
textbox2 = Pygame_Textinput.TextInput("", font_size=20, text_color=WHITE, cursor_color=ORANGE, max_string_length=18, rect=pygame.Rect(920, 385, 110, 30))

player_option = GUI.OptionBox(760, 435, 310, 30, GREY, (100, 200, 255), FONT, options)

options2 = []

die = Dice.Dice(pygame.Rect(1100+30, 50, 60, 60), pygame.Rect(1100+30, 120, 60, 60), BUTTON_COLOR, 30)


def redraw_window(WINDOW, player, player2, selected_option):
    WINDOW.fill(BLACK)
    WINDOW.blit(BOARD_IMG, board_rect)

    if player.is_player_turn(game.get_turn()):
        GUI.draw_text(WINDOW, "Turn: " + player.get_name(), FONT, WHITE, 50, 30, False)
    else:
        GUI.draw_text(WINDOW, "Turn: " + player2.get_name(), FONT, WHITE, 50, 30, False)

    info_rect = pygame.Rect(760, 435, 310, 300)
    pygame.draw.rect(WINDOW, WHITE, info_rect, 2)   # Player property box

    # Draw two players and their properties
    player.draw(WINDOW)
    player2.draw(WINDOW)

    if selected_option == player.get_turn_number():
        GUI.draw_text(WINDOW, "Wealth: $" + str(player.get_money()), FONT, WHITE, info_rect.left, 475, False)
        GUI.draw_text(WINDOW, "Properties: ", FONT, WHITE, info_rect.left, 510, False)
        player.draw_player_properties(WINDOW)
    else:
        GUI.draw_text(WINDOW, "Wealth: $" + str(player2.get_money()), FONT, WHITE, info_rect.left, 475, False)
        GUI.draw_text(WINDOW, "Properties: ", FONT, WHITE, info_rect.left, 510, False)
        player2.draw_player_properties(WINDOW)

    if player.is_player_turn(game.get_turn()) and player.get_position() in property_img:
        WINDOW.blit(property_img[player.get_position()], (785, 60))
    elif player2.is_player_turn(game.get_turn()) and player2.get_position() in property_img:
        WINDOW.blit(property_img[player2.get_position()], (785, 60))



    # Draw different buttons on the side
    draw_widgets()
    pygame.display.update()


def draw_widgets():
    roll_button.draw()
    die.draw_dice(WINDOW)
    buy_button.draw()
    build_button.draw()
    end_turn_button.draw()

    WINDOW.blit(textbox.get_surface(), (textbox.get_rect().left, textbox.get_rect().centery))
    WINDOW.blit(textbox2.get_surface(), (textbox2.get_rect().left, textbox2.get_rect().centery))

    player_option.draw(WINDOW)


def main():
    run = True
    clock = pygame.time.Clock()
    n = Network()
    p = n.get_p()
    switch_turn = False
    first = True

    while run:
        clock.tick(FPS)

        send_obj_list = []
        if switch_turn:
            send_obj_list = [p, str(game.get_turn()), die.get_dice_one(), die.get_dice_two()]
            switch_turn = False
        else:
            send_obj_list = [p, "x", die.get_dice_one(), die.get_dice_two()]

        data = n.send(send_obj_list)
        p2 = data[0]
        # print(str(data[1]))

        temp = game.get_turn()
        game.set_turn(int(data[1]))

        if p.is_player_turn(game.get_turn()) and temp != game.get_turn():
            player_option.set_selected_option(game.get_turn())

        die.set_dice(data[2], data[3])
        player_list = [p, p2]

        if first:
            options = []
            for _ in range(len(player_list)):
                options.append(None)
            for player in player_list:
                options[player.get_turn_number()] = player.get_name()
            first = False
            player_option.set_option_list(options)

            options2 = options.copy()
            options2.pop(p.get_turn_number())

        events = pygame.event.get()
        selected_option = player_option.update(events)

        for event in events:
            if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                if p.get_money != 0:
                    if not game.is_player_in_jail(p):
                        if roll_button.clicked(event) and not game.roll_complete():
                            die.roll()
                            game.set_dice_roll(die.dice_sum(), die.is_double(), p)
                            game.check_pos(p, player_list)

                        elif buy_button.clicked(event):
                            loc = property_dict["locations"][p.get_position()]["name"]
                            if p.is_player_turn(game.get_turn()) and loc not in special_locations:
                                pos = p.get_position()
                                col = property_dict["locations"][pos]["color"]
                                cost = property_dict["locations"][pos]["cost"]
                                p.buy_property(pos, col, cost)
                                break

                        elif build_button.clicked(event):
                            if p.is_player_turn(game.get_turn()):
                                pos = p.get_position()
                                col = property_dict["locations"][pos]["color"]
                                cost = property_dict["locations"][pos]["house_price"]
                                p.buy_house(pos, cost, col)
                                break

                        elif end_turn_button.clicked(event) and game.roll_complete():
                            game.change_turn()
                            switch_turn = True
                            player_option.set_selected_option(game.get_turn())
                else:
                    print(p.get_name() + " LOST!")
                    run = False

            elif event.type == pygame.QUIT:
                run = False

        redraw_window(WINDOW, p, p2, selected_option)

    pygame.quit()


main()




















