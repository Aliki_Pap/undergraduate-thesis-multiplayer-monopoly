import socket
import threading
import pickle
from player import Player
import time

HEADER = 64
PORT = 5555
SERVER = "192.168.1.106"
ADDRESS = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MSG = "DISCONNECTED!"

# create an INET, STREAMing socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    s.bind(ADDRESS)
except socket.error as e:
    str(e)

# become a server socket
s.listen(2)
print("Waiting for a connection... Server Started!")

connected = set()
idCount = 0

START_X, START_Y = 640, 670  # Starting position of a player
dog = Player("Dog", 1500, START_X, START_Y, 0)
hat = Player("Hat", 1500, START_X, START_Y, 1)
players = [dog, hat]
turn = 0
dice_rolls = ["?", "?", "0"]


def threaded_client(conn, player):
    conn.send(pickle.dumps(players[player]))
    reply = ""
    global turn

    while True:
        try:
            data = pickle.loads(conn.recv(1024*10))
            players[player] = data[0]

            if not data:
                print("Disconnected")
                break
            else:
                if player == 1:
                    reply = players[0]
                    if dice_rolls[2] == str(player):
                        if data[1] != "x":
                            dice_rolls[2] = data[1]
                            print("SWITCHING TURNS: ", dice_rolls[2])
                        dice_rolls[0] = data[2]
                        dice_rolls[1] = data[3]

                else:
                    reply = players[1]
                    if dice_rolls[2] == str(player):
                        if data[1] != "x":
                            dice_rolls[2] = data[1]
                            print("SWITCHING TURNS: ", dice_rolls[2])
                            dice_rolls[0] = data[2]
                            dice_rolls[1] = data[3]

            rep = [reply, dice_rolls[2], dice_rolls[0], dice_rolls[1]]
            conn.sendall(pickle.dumps(rep))

        except:
            break

    print("Lost Connection:")
    conn.close()


currentPlayer = 0

while True:
    conn, address = s.accept()
    print("Connected to:", address)

    threading.Thread(target=threaded_client, args=(conn, currentPlayer)).start()
    currentPlayer += 1
    print("Amount of players:", currentPlayer)
    



